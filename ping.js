var axios = require('axios');

let lastKnownPublicIP = "1.1.1.1";
const interval = 60;
const token = "cGVyZXN0cm9pa2Euc3BhY2UtdGltdXI6cGVyZXN0cm9pa2Euc3BhY2U=";
const hostname = "hq.perestroika.space"

const didPublicIpChanged = (publicIP) => {
    console.log("Current public IP", String(publicIP));
    console.log("Last known public IP", String(lastKnownPublicIP));


    if (String(lastKnownPublicIP) !== String(publicIP)) {
        console.log("IF( didPublicIpChanged ) => TRUE");
        return true
    } else {
        console.log("IF( didPublicIpChanged ) => FALSE");
        return false
    }
}

const main = async () => {

    console.log("Executing main procedure");

    fetchPublicIp()
        .then(function (publicIP) {
            console.log("Current public IP", publicIP);

            // todo: continue
        })

}

const fetchPublicIp = () => {
    console.log("Fetching public IP");

    return axios({
        method: 'get',
        url: `http://api.ipify.org`,
    })
        .then((response) => {
            return String(response.data)
        })
}

const updateDynDns = ({
    hostname, ip, authToken
}) => {
    console.log("Updating dyndns");

    axios({
        method: 'get',
        url: `https://www.ovh.com/nic/update?system=dyndns&hostname=${hostname}&myip=${ip}`,
        headers: {
            'Authorization': `Basic ${authToken}`
        }
    })
        .then(function (response) {
            console.log(JSON.stringify(response.data));
        })
        .catch(function (error) {
            console.log(error);
        });
}

main()